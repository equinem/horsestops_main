### README ###

# Horsestops: The website to find and offer Horse Stops. #

## How do I start development? ##
### Backend ###
* Requires Python3.5
* pip install -r requirements.txt
* pip install --upgrade git+https://github.com/gabn88/django-sorting
* ln -sT .env-example .env    # Some default settings
* ./backend/run-sev

### Frontend ###
* npm install
* npm run watch

## NOTE: For development, both the node server (npm run watch) and the django server (./run-sev) need to be running ##

## Contribution guidelines ##


### Some general information about the project ###
* Always use appending slash / in urls
* Always use www. prefix to access webapp and api. prefix to access api.