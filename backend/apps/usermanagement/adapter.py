from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings
from django.shortcuts import resolve_url
from django.utils import timezone
from django.template.base import TemplateDoesNotExist
from django.template.loader import render_to_string
from django.core.mail.message import EmailMultiAlternatives, EmailMessage
from django.utils.translation import ugettext_lazy as _

class AccountAdapter(DefaultAccountAdapter):
    ''' Change where to redirect to '''
    from django.contrib.auth.models import AbstractUser
    
    error_messages = {
        'invalid_username':
        _('Usernames can only contain letters, digits and @/./+/-/_.'),
        'username_blacklisted':
        _('Username can not be used. Please use other username.'),
        'username_taken':
        AbstractUser._meta.get_field('username').error_messages['unique'],
        'too_many_login_attempts':
        _('Too many failed login attempts. Try again later.'),
        'email_taken':
        _('This emailaddress is already used. You can try to login, or reset a forgotten password.'),
    }
    
    def get_login_redirect_url(self, request):
        
        assert request.user.is_authenticated()
        url = settings.LOGIN_REDIRECT_URL  # Fallback
        
        return resolve_url(url)
    
    def save_user(self, request, user, form, commit=True):
        """
        Saves a new `User` instance using information provided in the
        signup form.
        """
        from allauth.account.utils import user_username, user_email, user_field

        data = form.cleaned_data
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        email = data.get('email')
        username = data.get('username')

        user_email(user, email)
        user_username(user, username)
        if first_name:
            user_field(user, 'first_name', first_name)
        if last_name:
            user_field(user, 'last_name', last_name)
        if 'password1' in data:

            user.set_password(data["password1"])
            if data.get('agreed_to_terms_and_privacy', False):
                user.agreed_to_terms_and_privacy= timezone.now()
        else:
            user.set_unusable_password()
        self.populate_username(request, user)


        
        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user
    
    def render_mail_with_bcc(self, template_prefix, email, context, bcc):
        """
        Renders an e-mail to `email`.  `template_prefix` identifies the
        e-mail that is to be sent, e.g. "account/email/email_confirmation"
        """
        subject = render_to_string('{0}_subject.txt'.format(template_prefix),
                                   context)
        # remove superfluous line breaks
        subject = " ".join(subject.splitlines()).strip()
        subject = self.format_email_subject(subject)

        from_email = self.get_from_email()

        bodies = {}
        for ext in ['html', 'txt']:
            try:
                template_name = '{0}_message.{1}'.format(template_prefix, ext)
                bodies[ext] = render_to_string(template_name,
                                               context).strip()
            except TemplateDoesNotExist:
                if ext == 'txt' and not bodies:
                    # We need at least one body
                    raise
        if 'txt' in bodies:
            msg = EmailMultiAlternatives(subject,
                                         bodies['txt'],
                                         from_email,
                                         [email], [bcc])
            if 'html' in bodies:
                msg.attach_alternative(bodies['html'], 'text/html')
        else:
            msg = EmailMessage(subject,
                               bodies['html'],
                               from_email,
                               [email], [bcc])
            msg.content_subtype = 'html'  # Main content is now text/html
        return msg

    def send_mail(self, template_prefix, email, context, bcc=None):
        if not bcc:
            msg = self.render_mail(template_prefix, email, context)
        else:
            msg = self.render_mail_with_bcc(template_prefix, email, context, bcc)
        msg.send()
