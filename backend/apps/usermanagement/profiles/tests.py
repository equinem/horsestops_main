
from django.test import TestCase

#Third-party app imports


from apps.usermanagement.tests import RegistrationTests


class ProfileTest(TestCase):
    def setUp(self):
        self.user, self.password = RegistrationTests.verified_user()
        self.logged_in = self.client.login(username=self.user.username, password=self.password)
        self.stable = self.user.stableuser.stable.first()
        
    def test_login_user(self):
        self.assertTrue(self.logged_in)
        
    def test_profile_view(self):
        # Create an instance of a GET request.
        response = self.client.get('/equinem/profile/')
        #request.user = self.user
        # Test my_view() as if it were deployed 
        #response = user_profile(request)
        self.assertEqual(response.status_code, 200)
        