from django.utils.cache import patch_vary_headers
from django.utils import translation, timezone
from .models import Profile
import datetime
from django.core.cache import cache
from django.core.urlresolvers import is_valid_path, get_script_prefix
from django.conf import settings
from django.middleware.locale import LocaleMiddleware



class UpdateLastActivityMiddleware(object):

    KEY = "last-activity"

    def process_request(self, request):
        assert hasattr(request, 'user'), \
            'The UpdateLastActivityMiddleware requires authentication middleware to be installed.'
    
        if request.user.is_authenticated():
            cache_key = self.KEY+':%s' % (request.user.pk)
            last_activity = cache.get(cache_key)  # can be -1 if not found
            # If key is old enough, update database.
            too_old_time = timezone.now() - datetime.timedelta(minutes=15)
            if not last_activity or last_activity == -1 or last_activity < too_old_time:
                Profile.objects.filter(user__id=request.user.id).update(
                        last_activity=timezone.now())

            cache.set(cache_key, timezone.now(), 900)

        return None             


                           
class UserLocaleMiddleware(LocaleMiddleware):
    """
    This is a very simple middleware that parses a request
    and decides what translation object to install in the current
    thread context depending on the user's account. This allows pages
    to be dynamically translated to the language the user desires
    (if the language is available, of course).
    """

    def get_language_for_user(self, user):
        language = cache.get('language_for_user_{}'.format(user.id))
        if not language:
            try:
                profile = Profile.objects.get(user=user)
                if profile.language:
                    cache.set('language_for_user_{}'.format(user.id), profile.language)
                    return profile.language
                
            except Profile.DoesNotExist:
                pass
        else:
            return language

    
    def get_language_from_request(self, request, check_path):
        if request.user.is_authenticated():
            return self.get_language_for_user(request.user)
        else:
            return translation.get_language_from_request(request, check_path=check_path)

    def process_request(self, request):
        check_path = self.is_language_prefix_patterns_used()
        # the get_response in newer middleware django 1.10       
        language = self.get_language_from_request(request, check_path = check_path)
        translation.activate(language)
        request.LANGUAGE_CODE = translation.get_language()

    def process_response(self, request, response):
        language = translation.get_language()
        language_from_path = translation.get_language_from_path(request.path_info)
        if (response.status_code == 404 and not language_from_path
                and self.is_language_prefix_patterns_used()):
            urlconf = getattr(request, 'urlconf', None)
            language_path = '/%s%s' % (language, request.path_info)
            path_valid = is_valid_path(language_path, urlconf)
            if (not path_valid and settings.APPEND_SLASH
                    and not language_path.endswith('/')):
                path_valid = is_valid_path("%s/" % language_path, urlconf)

            if path_valid:
                script_prefix = get_script_prefix()
                language_url = "%s://%s%s" % (
                    request.scheme,
                    request.get_host(),
                    # insert language after the script prefix and before the
                    # rest of the URL
                    request.get_full_path().replace(
                        script_prefix,
                        '%s%s/' % (script_prefix, language),
                        1
                    )
                )
                return self.response_redirect_class(language_url)

        if not (self.is_language_prefix_patterns_used()
                and language_from_path):
            patch_vary_headers(response, ('Accept-Language',))
        if 'Content-Language' not in response:
            response['Content-Language'] = language
        return response