from django.shortcuts import redirect, render
from equinem.stables.models import Stable
import pytz
from apps.usermanagement.profiles.forms import ProfileForm, customUserForm
from django.contrib import messages
from apps.usermanagement.profiles.models import Profile
from django.views.i18n import set_language
from django.utils.translation import check_for_language, get_language, \
    get_language_info, activate
from framework.general.helper import custom_permission_denied

from django.contrib.auth import get_user_model

from django.utils.translation import gettext as _

from django.contrib.auth.decorators import login_required
from django.conf import settings
from apps.usermanagement.customuser.forms import UserInformationForm
from apps.usermanagement.customuser.models import UserInformation
from django.core.cache import cache
from django.core.urlresolvers import reverse


# Create your views here.
@login_required
def user_profile(request, user_id=None, form_id=None):

    active_tab = request.GET.get('active_tab', 'menu1')

    if user_id:
        user = get_user_model().objects.get(pk=user_id)
        profile = Profile.objects.get_or_create(user=user, defaults={'language':get_language()})[0]
    elif request.user.is_authenticated():
        user = request.user
        profile = Profile.objects.get_or_create(user=user, defaults={'language':get_language()})[0]
    else:
        user = None
        raise messages.error('User cannot be found.')
    
    if not request.user.has_perm('profiles.view_profile', profile):
        return custom_permission_denied(request, _('U heeft geen toegang tot het profiel van {user}').format(user=user))
    

    pForm = ProfileForm(request.POST or None, instance=profile, prefix='ProfileForm')    
    uForm = customUserForm(request.POST or None, instance= user, prefix='UserForm')
    uiForm = UserInformationForm(request.POST or None, instance = UserInformation.objects.get_or_create(user=user)[0])

    if all([pForm.is_valid(), uForm.is_valid(), uiForm.is_valid()]):
        pForm.save(request.POST)
        uForm.save()
        uiForm.save(user=user)
        messages.success(request, _('Profiel van {user} succesvol geupdate').format(user=user))
        
    
    context = {'user': user, 'ProfileForm':pForm,
               'uForm':uForm, 'uiForm':uiForm,
            'available_languages': settings.SELECTABLE_LANGUAGES, 'active_tab':active_tab}
    template = 'apps.usermanagement/profiles/profile.html'
    

    context.update({'redirect_to': reverse('profile')})   # passed via 'next' in the form to set_language
    
    return render(request, template, context)



def set_timezone(request):
    tz_name = request.POST.get('timezone', None)
    if request.method == 'POST' and tz_name:
        request.session['django_timezone'] = request.POST['timezone']
        stable = Stable.objects.get_only_one_from_request(request)
        if stable:
            stable.timezone = pytz.timezone(tz_name)
            stable.save()
            messages.success(request, _('De tijdszone is succesvol aangepast voor stal {}.').format(stable))            
        else:
            messages.success(request, _('De tijdszone is succesvol aangepast voor deze sessie.'))
        return redirect(request.META.get('HTTP_REFERER', '/')) 
    elif request.method == 'POST' and not tz_name:
        messages.error(request, _('De tijdszone kon niet worden aangepast'))    
        return redirect(request.META.get('HTTP_REFERER', '/'))    
    else:
        return render(request, 'apps.usermanagement/timezone/timezone_template.html', {'timezones': pytz.common_timezones})
    
    
def custom_set_language(request):
    lang_code = request.POST.get('language', None)

    if request.user.is_authenticated() and request.method == 'POST':
        if lang_code and check_for_language(lang_code):
            cache.delete('language_for_user_{}'.format(request.user.id))
    
            profile = Profile.objects.get(user=request.user)
            profile.language = lang_code
            profile.save()

    response = set_language(request)
    if not lang_code:
        lang_code = get_language()
        language = get_language_info(get_language())
        messages.info(request, _('De huidige taal is {}').format(language['name_local']))
    else:
        language = get_language_info(lang_code)
        activate(lang_code)
        messages.success(request, _('Taal succesvol aangepast naar {}').format(language['name_local']))
    return response