from django.db import models
from django.conf import settings

from django.utils.translation import ugettext_lazy as _, get_language


# Create your models here.

#import stripe
from django.contrib.auth.models import Group

from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
#from colorfield.fields import ColorField as RGBColorField
from colorful.fields import RGBColorField
from multiselectfield.db.fields import MultiSelectField


#stripe.api_key = settings.STRIPE_SECRET_KEY


class ProfileQuerySet(models.QuerySet):
    def active(self):
        ''' Shows only the profiles of the users that are active '''
        return self.filter(user__is_active=True)
    


class ProfileManager(models.Manager):
    def get_queryset(self):
        return ProfileQuerySet(self.model, using=self._db)
    
    def active(self):
        return self.get_queryset().active()
    


class Profile(models.Model):
    
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    desciption = models.TextField(_('beschrijving'), default=_('My description'), blank=True)
    ref_id = models.CharField(max_length=120, blank=True)    # str(uuid.uuid4())[:14].replace('-','')
    ip_address = models.CharField(max_length=120,  blank=True)
    initials = models.CharField(_('Initialen'), help_text=_('Maximaal drie letters'),max_length=3, blank=True)
    last_activity = models.DateTimeField(_('laatste activiteit'), null=True,blank=True)
    language = models.CharField(_('Taal'), max_length=10, blank=True) # A user doesn't need a language to be set explicitly  (zh-hans is largest locale?)
    
    objects=ProfileManager()

    color = RGBColorField(_('kleur'), default='#800000')


    def get_full_name(self):
        if not self.user.first_name:
            return self.user.username
        else:
            return ' '.join([self.user.first_name, self.user.last_name])

    def __unicode__(self):
        return self.get_full_name()
        
    def short(self):
        if self.initials:
            return self.initials
        else:
            return self.get_full_name()[0:3]

    