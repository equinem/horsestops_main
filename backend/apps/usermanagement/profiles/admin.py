from django.contrib import admin


# Register your models here.
from .models import Profile
from horsestops.admin_helper import UserAdminForm

class ProfileAdmin(admin.ModelAdmin):  #Make GuardedModelAdmin in the future
    list_display = ('__str__', 'user', 'last_activity')
    form = UserAdminForm
    class Meta:
        model = Profile
        
admin.site.register(Profile, ProfileAdmin)

'''
class UserStripeAdmin(GuardedModelAdmin):
    class Meta:
        model = UserStripe

admin.site.register(UserStripe,UserStripeAdmin)
'''