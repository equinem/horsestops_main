from django import forms
from .models import Profile
from django.forms.fields import CharField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from equinem.horses.models import HorseGroup
import colorful
from colorful.forms import RGBColorField
from equinem.stables.models import StableUser, GroupStable, Stable
from framework.general.widgets import MultiStateSelectWidget
from allauth.account.models import EmailAddress
from equinem.activities.models import ExtraInfoActivity

# The UserProfile Form


class customUserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(customUserForm, self).__init__(*args, **kwargs)
        
        for field in list(self.fields):
            self.fields[field].required = True
        
    
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name')
        error_messages = {
        }
        exclude = ()
        
