from django.apps import AppConfig
from django.db.models.signals import post_save

class ProfilesConfig(AppConfig):
    name = 'apps.usermanagement.profiles'
    verbose_name = 'Profiles'
    
    def ready(self):   
        pass