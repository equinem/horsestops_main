from django.utils import timezone
from horsestops.general.helper import get_tz

class TimezoneMiddleware(object):
    def process_request(self, request):
        tz = get_tz(request)  
        if tz:
            timezone.activate(tz)
            request.session['django_timezone'] = tz.zone
        else:
            timezone.deactivate()  # sets the timezone to settings.TIMEZONE
        
            
            
            
            