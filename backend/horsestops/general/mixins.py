from django import http
import json
from dal.views import BaseQuerySetView
from dal_select2.views import Select2ViewMixin
from django.utils.translation import gettext as _


class RedirectSelect2ViewMixin(Select2ViewMixin):
    def render_to_response(self, context):
        """Return a JSON response in Select2 format."""
        create_option = []

        q = self.request.GET.get('q', None)

        display_create_option = False
        if (self.create_field or self.hyperlinked_create_field) and q:
            page_obj = context.get('page_obj', None)
            if page_obj is None or page_obj.number == 1:
                display_create_option = True
                

                
        if display_create_option:
            if self.create_field:
                create_option = [{
                    'id': q,
                    'text': _('Create "%(new_value)s"') % {'new_value': q},
                    'create_id': True,
                }]
            elif self.hyperlinked_create_field:
                create_option = [{
                    'redirect': str(self.hyperlinked_create_field),
                    'id': q,
                    'text': _('Create "%(new_value)s"') % {'new_value': q},
                    'create_id': True,
                }]

        return http.HttpResponse(
            json.dumps({
                'results': self.get_results(context) + create_option,
                'pagination': {
                    'more': self.has_more(context)
                }
            }),
            content_type='application/json',
        )

class RedirectCreationSelect2QuerySetView(RedirectSelect2ViewMixin, BaseQuerySetView):
    hyperlinked_create_field = None