### Helper variables and functions ###
import pytz

from django.utils.six.moves.urllib.parse import urlparse
from django.utils.translation import ugettext as _
from django import forms
import datetime
import sys
from django.http import HttpResponse
from django.utils.html import escape, escapejs
from django.shortcuts import render, _get_queryset
from django.core import exceptions
from django.template.loader import render_to_string
from django.utils.timezone import is_aware, is_naive
from django.utils.encoding import smart_text
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model, REDIRECT_FIELD_NAME
from django.template.defaultfilters import length
from django.contrib.auth.decorators import user_passes_test, login_required
from django.views.defaults import permission_denied
from horsestops.general.forms import deleteForm
from django.contrib import messages
from distutils.util import strtobool
from dateutil.relativedelta import relativedelta

from django.utils.decorators import available_attrs
from functools import wraps
from django.shortcuts import resolve_url
from django.conf import settings

if sys.version_info > (3,):
    long = int


EU_COUNTRY_LIST = ['BE','BG','CZ','DK','DE','EE','IE','EL','ES','FR','HR','IT','CY','LV','LT','LU','HU','MT','NL','AT','PL','PT','RO','SI','SK','FI','SE','UK']

# Some helper stuff for timezones (might be helpfull later, not used yet)
local_tz = pytz.timezone(settings.TIME_ZONE) # use your local timezone name here.... For now the timezone in the Django settings.py is used. Better is to ask users of their stable location and use that timezone.
localFormat = "%d-%m-%Y %H:%M:%S"     # The format used for showing time in local conventions.
from django.utils import timezone, six, dates
import logging
logger = logging.getLogger(__name__)

try:
    from django import get_app, get_models

    def get_models_from_app(appname):
        '''
        This function gets the models for a specific app.
        An app is for example: 'activities', 'sticky_notes', etc.
        '''
        app = get_app(appname)
        return get_models(app)

    def get_models_from_app_list(app_list):
        '''
        Needs a list with appnames
        Gives back a list with models
        '''
        model_list=[]
        for app in app_list:
            model_list.extend(get_models_from_app(app))
        return model_list

except ImportError:
    from django.apps import apps
    def get_models_from_app_list(app_list):
        '''
        Needs a list with appnames
        Gives back a list with models
        '''
        model_list=[]
        for app in app_list:
            app_config = apps.get_app_config(app)
            model_list.extend(app_config.get_models())
        return model_list


def request_passes_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Decorator for views that checks that the request passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the request object and returns True if the request passes.
    """

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request):
                return view_func(request, *args, **kwargs)
            path = request.build_absolute_uri()
            resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if ((not login_scheme or login_scheme == current_scheme) and
                    (not login_netloc or login_netloc == current_netloc)):
                path = request.get_full_path()
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(
                path, resolved_login_url, redirect_field_name)
        return _wrapped_view
    return decorator


def custom_permission_denied(request, message):
    if request.is_ajax():
        return form_fail(request, message)
    else:
        return permission_denied(request)

'''
def stable_active_required(request):
    from equinem.stables.models import Stable
    ""
    Decorator that checks if the Stable of the user is paid for, redirecting to the payment
    page if necessary.
    ""
    stable = Stable.objects.get_only_one_from_request(request)
    if stable:
        return stable.is_paid
    else:
        return False

active_subscription_required = request_passes_test(stable_active_required,
                                   login_url='/payment')

def active_subscription_and_login_required(view_func):
    decorated_view_func = login_required(active_subscription_required(view_func))
    return decorated_view_func

'''

def get_object_or_none(classmodel, *args, **kwargs):
    """
    Uses get() to return an object, or returns None if the object
    does not exist.

    klass may be a Model, Manager, or QuerySet object. All other passed
    arguments and keyword arguments are used in the get() query.

    If a Models is passed, the default manager is taken (which is the upper one).

    Note: Like with get(), an MultipleObjectsReturned will be raised if more than one
    object is found.
    """
    queryset = _get_queryset(classmodel)
    try:
        return queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        return None

# Find the start and end of the week based on the (ISO) weeknumber
def iso_year_start(iso_year):
    """The gregorian calendar date of the first day of the given ISO year"

    Parameters:
            datetime local_dt_unaware
                    Local date and time without tz info.

    Returns:
            datetime    Date and time in universal time with correct timezone info.

    Needed for:
            iso_to_gregorian()
    """

    fourth_jan = datetime.date(iso_year, 1, 4)
    delta = datetime.timedelta(fourth_jan.isoweekday()-1)
    return fourth_jan - delta

def iso_to_gregorian(iso_year, iso_week, iso_day):
    """Gregorian calendar date for the given ISO year, week and day"

    Parameters:
            datetime local_dt_unaware
                    Local date and time without tz info.

    Returns:
            datetime    Date and time in universal time with correct timezone info.
    """

    year_start = iso_year_start(iso_year)
    return year_start + datetime.timedelta(days=iso_day-1, weeks=iso_week-1)

def daterange(start_date, end_date):
    """ Gives back a list of datetime objects with the given range (including endpoints)

    Parameters:
            datetime start_date
                    start of the range
            datetime end_date
                    end of the range

    Returns:
            list lst
                    List of datetime objects
    """
    lst=[]
    for n in range(int ((end_date - start_date).days)+1):     # +1 to INCLUDE end date!!!!
        lst.append(start_date + datetime.timedelta(n))
    return lst

def get_local_date(datetime_obj):
    ''' Returns the local (in Stable-Time) date, also if an aware datetime is given
    input: datetime_obj
                        Can be aware datetime or date object
                        If date object, a local date is already assumed
    '''
    current_tz = timezone.get_current_timezone()
    if isinstance(datetime_obj, datetime.datetime):
        if is_aware(datetime_obj):
            local = current_tz.normalize(datetime_obj.astimezone(current_tz))
            return local.date()
        if is_naive(datetime_obj):

            raise exceptions.ValidationError(
                'invalid_date given, probably a naive datetime? Please contact Equinem with this error message and it will get fixed.'
            )

    elif isinstance(datetime_obj, datetime.date):
        return datetime_obj

def get_local_week(datetime_obj):
    ''' Return local year and weeknumber '''
    current_tz = timezone.get_current_timezone()
    local = current_tz.normalize(datetime_obj.astimezone(current_tz))
    return local.isocalendar()[0], local.isocalendar()[1]

def replace_in_local(datetime_obj, year=None, week=None,month=None,day=None):
    ''' In calendars, when you want to go from one date to another, you replace the month and day according to the users want.
    I.e. the user types in equinem/day/2015/5/5 and you want to go to year 2015, month 5 and day 5 in LOCAL time.
    This function helps with a replace function that replaces the year/week/day in LOCAL time.
    '''
    current_tz = timezone.get_current_timezone()
    if isinstance(datetime_obj, datetime.datetime):
        if is_aware(datetime_obj):
            datetime_obj = current_tz.normalize(datetime_obj.astimezone(current_tz))

    if week:
        pass
    else:
        if year:
            datetime_obj = datetime_obj.replace(year=year)
        if month:
            datetime_obj = datetime_obj.replace(month=month)
        if day:
            datetime_obj = datetime_obj.replace(day=day)

    return datetime_obj.astimezone(pytz.utc)



def get_tz(request):
    tzname = request.session.get('django_timezone') or None
    if tzname:
        tz = pytz.timezone(tzname)
    else:
        tz = None

        # If timezone cannot be get from request, try to get it via the user and his Stable. (first stable is taken).
        if not tz:
            try:

                tz = request.user.timezone
            except:
                tz = None
        else:
            tz = None
    return tz


def reminders(request, horses):
    """Return the list of reminders for today and the next six days for the given horse(s).

    Parameters:
            request
                    request
            horses
                    end of the range

    Returns:
            list lst
                    List of datetime objects
    """
    today = timezone.now().date()
    end_range = today + datetime.timedelta(days=14)
    start_range = today- relativedelta(months=1)

    #RealActivities=get_model('horses', 'RealActivities')  this is the same as below:
    from equinem.activities.models import RealActivities
    from hr_management.views import CustomDate

    from equinem.activities.models import ActivityType
    
    # select activity_type__stable is for the get_absolute_url function
    activity_types = ActivityType.objects.order_by().for_stable(request.stable).get_objects_for_user(request.user, 'activities.view_activitytype')
    reminders = RealActivities.objects.between_dates(start_range, end_range).for_horses(horses).for_activity_type(activity_types).filter(remind=True, done_by__isnull=True).select_related('horse').select_related('activity_type__stable').select_related('activity_type').order_by('activity_type', 'date')
    new_activity_type_list = []
    old_activity_type = False
    changed_activity_type = True
    loopdate = False

    for reminder in reminders:

        changed_activity_type = False
        changed_attr = False


        if reminder.date < timezone.now().astimezone(request.stable.timezone).date():
            attr = 'overdue_dates'
        elif reminder.date >= timezone.now().astimezone(request.stable.timezone).date():
            attr = 'coming_dates'


        activity_type = reminder.activity_type


        if reminder.activity_type != old_activity_type: # takes the old activity_type
            new_activity_type_list.append(activity_type)
            changed_activity_type = True

        if not hasattr(new_activity_type_list[-1], attr):
            setattr(new_activity_type_list[-1], attr, [])


        if reminder.date != loopdate or changed_activity_type or changed_attr:
            loopdate = reminder.date
            new_date = CustomDate(reminder.date.year, reminder.date.month, reminder.date.day)
            setattr(new_date, 'activities', [])
            dates_list = getattr(new_activity_type_list[-1], attr)
            dates_list.append(new_date)

        dates_list = getattr(new_activity_type_list[-1], attr)
        activities_for_date = getattr(dates_list[-1], 'activities')
        activities_for_date.append(reminder)

        old_activity_type = activity_type

    return new_activity_type_list

def form_save_and_succes(request, form, is_ajax, **kwargs):
    ''' Tries to safe the form and display the success message via the bootstrap modal.
    Requires:
        form
            the form to save (all fields should be made before)
        is_ajax
            boolean if the request is an ajax request (True) or not (False)
    '''
    new_instance = False
    try:
        pre_instance = form.save(commit=False)
        if not pre_instance.pk:
            new_instance = True
        modelInstance=form.save()
    except forms.ValidationError:
        modelInstance = None
    if modelInstance and is_ajax:
        if new_instance:
            return message_response(request, 'add', modelInstance)
        else:
            return message_response(request, 'change', modelInstance)

    if modelInstance and not is_ajax:
        return HttpResponse(
            '<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>' % \
            (escape(modelInstance._get_pk_val()), escapejs(modelInstance)))


def form_response(request, body_text, alert, title_text=None,  reload=True, redirect=None, reload_timeout=2000):
    ### Somethow this should be implemented in the future
    #if request.is_ajax():
    #    return HttpResponse(_('U heeft geen rechten om schema\'s te verwijderen.'), status=403)
    #else:
    #import ipdb;ipdb.set_trace()
    body_text_to_form=False
    try:
        # messages exists
        body_text_to_form = body_text.messages
    except:
        pass
    if not body_text_to_form:
        try:
            # messages exists
            body_text_to_form = body_text.detail
        except:
            pass
    if not body_text_to_form:
        if not isinstance(body_text, list):
            body_text_to_form = [body_text]
            alert = [alert]
        else:
            body_text_to_form = body_text

    final_body_text = []
    for index, body in enumerate(body_text_to_form):

        try:
            final_body_text.append([body, alert[index]])
        except:
            final_body_text.append([body, alert])

    pageContext = {'title_text':title_text,
                          'body_text':final_body_text, 'reload':reload, 'redirect':redirect, 
                          'reload_timeout':reload_timeout}

    if request.is_ajax():
        return HttpResponse(render_to_string('horsestops/form/form_response.html',
                                                     pageContext))
    else:
        raise ValidationError(_('Dit is nog niet geimplementeerd.'))

def form_fail(request, body_text, title_text=None, alert=None, reload=True, redirect=None):

    if not title_text:
        title_text = _('Toegang geweigerd')

    return form_response(request, body_text, title_text=title_text, alert='warning', reload=True, redirect=redirect)

def message_response(request, action, modelInstanceName, redirect=None, body_text=None, reload=True):
    ''' The succes form for different types (deletion/changes/add).
    Returns an HttpResponse (which is just a page reload).

    Requires:
        action
            type of action that has been performed ('delete', 'change', 'add')
        modelInstanceName
            how you would like to display the modelinstance object
        redirect
            to where should be redirected (if empty, the same page is reloaded)
    '''




    try:
        mI_name = modelInstanceName.name
    except:
        mI_name = modelInstanceName

    if 'delete' in action:
        title_text = _('Verwijderen succesvol')
        if not body_text:
            body_text = _('{0} is succesvol verwijderd').format(mI_name)
    elif 'change' in action:
        title_text= _('Wijzigen succesvol')
        if not body_text:
            body_text = _('{0} is succesvol gewijzigd').format(mI_name)

    elif 'add' in action:
        title_text = _('Toevoegen succesvol')
        if not body_text:
            body_text = _('{0} is succesvol toegevoegd').format(mI_name)



    alertType= 'success'
    body_text_to_form = [[body_text, alertType]]        # just a single item, therefore double braces...
    for body, alert in body_text_to_form:
        messages.success(request, body)

    pageContext = {'title_text':title_text,
                              'body_text':body_text_to_form,
                              'redirect':redirect, 'reload':reload}

    return HttpResponse(render_to_string('horsestops/form/reload_page.html',
                                                     pageContext))


def popupResponse(request, title_text, body_text, alert, redirect=None):
    body_text_to_response=False
    try:
        # messages exists
        body_text_to_response = body_text.messages
    except:
        pass
    if not body_text_to_response:
        try:
            # messages exists
            body_text_to_response = body_text.detail
        except:
            pass
    if not body_text_to_response:
        if not isinstance(body_text, list):
            body_text_to_response = [body_text]
        else:
            body_text_to_response = body_text

    final_body_text = []
    for index, body in enumerate(body_text_to_response):

        try:
            final_body_text.append([body, alert])
        except:
            final_body_text.append([body, alert])

    pageContext = {'title_text':title_text,
                          'body_text':final_body_text, 'redirect':redirect}

    if request.is_ajax():
        return HttpResponse(render_to_string('horsestops/form/popup_response.html',
                                                     pageContext))


def handlePopDelete(request,  modelInstance, title_text, extra_text, redirect=None, override_delete=None):
    """Deze functie zorgt ervoor dat na het verwijderen eerst om confirmation wordt gevraagd.
    De functie gaat ervan uit dat dit op dezelfde url gedaan wordt( request.path)

    Parameters:
            request
                De HTTP request info
            field
                de model instance die verwijderd dient te worden
    Returns:
            a popup window
    """

    def deletestuff(modelInstance):
        from apps.usermanagement.helper_auth import delete_user_from_stable

        if isinstance(modelInstance, (list, tuple)):
            delObject = []
            lenlist= length(modelInstance)
            for unpackedInstance in modelInstance:
                delObject.append(deletestuff(unpackedInstance))
            if lenlist == sum(delObject):
                delObject = 1
            else:
                delObject = None
        else:
            try:
                modelInstance.hide()
                delObject = 1
            except forms.ValidationError: #, AssertionError
                raise
            except AttributeError:
                modelInstance.delete()
                logger.debug('The deleteForm deleted {0}, don\'t you want an inactive method?'.format(modelInstance))
                delObject = 1
            except:
                raise

        return delObject




    form = deleteForm(request.POST or None)
    context = {'redirect':redirect}
    if request.is_ajax():
        if form.is_valid() and request.POST.get('delete') == '1':
            if not override_delete:
                try:
                    deletestuff(modelInstance)
                    return message_response(request, 'delete', modelInstance ,redirect )
                except ValidationError as e:
                    return form_fail(request, e, _('Verwijderen mislukt'))

            else:
                return 'user_clicked_delete'
        context.update({'form': form, 'title_text':title_text, 'url_address':request.get_full_path(), 'extra_text':extra_text})
        return render(request, 'horsestops/form/popupFieldDelete.html', context)
    else:
        if form.is_valid() and request.POST.get('delete') == '1':
            if not override_delete:
                try:
                    delObject = deletestuff(modelInstance)
                    if delObject:
                        return HttpResponse(
                            '<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>')
                except ValidationError as e:
                    messages.error(request, _('Verwijderen mislukt. ') + ', '.join(e.messages))
                    return HttpResponse(
                        '<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>')
                    
            else:
                return 'user_clicked_delete'
        context.update({'form': form, 'title_text': title_text, 'url_address':request.get_full_path(), 'extra_text':extra_text})

        return render(request, 'horsestops/form/popupformdelete.html', context)






def beginning_of_week(day):
    ''' function to get the first day of the week
    input:
        day = datetime or timezone.now (WITHOUT PARENTHESES, so it will be new every time)
    '''
    try:
        day_of_week = day.weekday()
    except AttributeError:
        day_of_week = day().weekday()
        day = day()
    to_beginning_of_week = datetime.timedelta(days=day_of_week)
    beginning_of_week = day - to_beginning_of_week
    return beginning_of_week



'''    #####  some definitions are below  ######     '''
"""Creates an array with daynames of a week and abbreviated day names. Also an array for the monthnames is created.

Parameters:
    Which locale do you want to set?

Returns:
    array   week
        Weekdays of a week in the given locale
    array   week_abbr
        Abbreviated weekdays of a week in the given locale
    array   yearname
        Names of months in the given locale
"""


def get_week_abbr():
    week_abbr = []
    for n in range(0,7):
        #week_full += (calendar.day_name[n], )
        week_abbr.append( dates.WEEKDAYS_ABBR[n])
    return week_abbr

'''    #####  end of definitions...  ######     '''




def user_yes_no_query(question):
    sys.stdout.write('%s [y/n]\n' % question)
    while True:
        try:
            return strtobool(input().lower())
        except ValueError:
            sys.stdout.write('Please respond with \'y\' or \'n\'.\n')


'''   ###################################################################### HELPER CLASS: BitChoices ##################################################'''
class BitChoices(object):
    def __init__(self, choices):
        self._choices = []
        self._keys = []
        self._lookup = {}
        for index, (key, val) in enumerate(choices):
            index = 2**index
            self._choices.append((index, val))
            self._keys.append((index, key))
            self._lookup[key] = index

    def __iter__(self):
        return iter(self._choices)

    def __len__(self):
        return len(self._choices)

    def __getattr__(self, attr):
        try:
            return self._lookup[attr]
        except KeyError:
            raise AttributeError(attr)

    def get_selected_keys(self, selection):
        """ Return a list of keys for the given selection, or empty is there is no selection or empty selection."""
        if selection:
            return [k for k,b in six.iteritems(self._lookup) if b & selection]

        else:
            return []

    def get_selected_names(self, selection):
        """ Return a list of names for the given selection, or empty is there is no selection or empty selection """
        if selection:
            return [v for b, v in self._choices if b & selection]
        else:
            return []

    def get_selected_values(self, selection):
        """ Return a list of values for the given selection, or empty is there is no selection or empty selection """
        if selection and isinstance(selection, (int, long)):
            return [b for b, dummy in self._choices if b & selection]
        elif isinstance(selection, list):
            return selection
        else:
            return []

    def list_of_keys(self):
        "Return list of all keys"
        return [smart_text(self._keys[i][1]) for i in range(0, len(self))]
