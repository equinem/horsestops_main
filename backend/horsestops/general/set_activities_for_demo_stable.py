from equinem.activities.models import ActivityType, RealActivities
import random
from equinem.stables.models import Stable
from equinem.horses.models import Horse
import datetime
from horsestops.general.helper import daterange

def set_activity_types_for_between_range(start=None, end = None):
    '''
    The below code is not working, because it sets multiple times rijden, i.e. per day
    '''
    stable = Stable.objects.get(slug='stal-de-demohoeve')
    
    horses = Horse.objects.for_stable(stable)
    
    activity_types = ActivityType.objects.for_stable(stable)
    rijden = activity_types.get(default_id=1)
    paddock = activity_types.get(name__icontains='paddock')
    try: 
        wei = activity_types.get(name__icontains='wei')
        not_used_already = activity_types.exclude(pk=wei.pk)
    except:
        wei = None
        not_used_already = activity_types
        
    not_used_already = not_used_already.exclude(pk=rijden.pk).exclude(pk=paddock.pk)
    
    if not start or not end:
        Exception('please enter start and end date')
        
    
    def random_date_within_range(start, end):
        return start + (end - start) * random.random()
    
    
    for horse in horses:
        for i in range(5):
            RealActivities.objects.create(horse=horse, activity_type=rijden, date=random_date_within_range(start, end))
        if wei:
            for i in range(3):
                RealActivities.objects.create(horse=horse, activity_type=wei, date=random_date_within_range(start, end))                

        for i in range(3):
            RealActivities.objects.create(horse=horse, activity_type=paddock, date=random_date_within_range(start, end))                
            
            
        for i in range(2):
            rndom_activity_type = not_used_already.order_by('?').first()
            RealActivities.objects.create(horse=horse, activity_type=rndom_activity_type, date=random_date_within_range(start, end))

def copy_activity_types_from_to(from_start, from_end, to_start): 
    stable = Stable.objects.get(slug='stal-de-demohoeve')
    horses = Horse.objects_with_inactive.for_stable(stable)
    range_dates = daterange(from_start, from_end)
    
    for i, date in enumerate(range_dates):
        activities_for_date=RealActivities.objects.filter(date=date, horse=horses)
        
        for activity in activities_for_date:
            activity.pk = None
            activity.date = to_start+datetime.timedelta(days=i)
            try:
                activity.save()
            except:
                pass
        
    
    
    