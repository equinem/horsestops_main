import datetime
from django.utils import six
from rest_framework.utils import encoders
from rest_framework.renderers import JSONRenderer



# below the JSONEncoder used in SRF TimezoneField
class JSONEncoder(encoders.JSONEncoder):
    '''
    This is so that the TimezoneField works again
    '''
    def default(self, obj):
        if isinstance(obj, datetime.tzinfo):
            return six.text_type(obj)
        return super(JSONEncoder, self).default(obj)

class JSONRenderer(JSONRenderer):
    encoder_class=JSONEncoder

