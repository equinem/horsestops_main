from django.utils.encoding import force_text
from horsestops.general.helper import form_save_and_succes, form_response,\
    message_response
from django.utils.translation import ugettext as _
from django.shortcuts import render
from django.http.response import HttpResponse
from django.utils.html import escape, escapejs
from django.core.exceptions import ImproperlyConfigured


class PopUpForm():
    """Deze class zorgt voor afhandeling van de popup, bv na  succesvol saven in de popup sluit deze automatisch en wordt dit in de select van het parent venster wordt geupdate.
    De functie werkt ook met ajax requests voor in-page (css) popups.
    Als je een object wilt wijzigen, ipv aanmaken, zul je dat object in de form.instance moeten hebben staan.
    
    LET OP: Aangezien meerdere forms vaak een parent - child structuur zullen hebben, moet de form_list met de hoogste parent beginnen en dan steeds verder naar beneden gaan tot de laagste child.
    
    LET OP2: Elke keer als je de form aanroept, moet je de popupform functie ook aanroepen! Dit i.v.m. het (opnieuw) setten van de prefix.
    
    Parameters:
            request
                De HTTP request info
            form
                De form die gebruikt moet worden
                Als meerdere forms gebruikt worden, maak dan een lijst van de form objecten en dit wordt automatisch gedetecteerd
            possible kwargs (noodzakelijk als er meerdere forms in een lijst staan!):

            field
                De naam van het field, zoals deze weergeven moet worden: b.v. "activity type"
      
            title_text
                De titel text, deze overschrijft de variabele field en veranderd de submit button naar wijzig
                
            url_address
                De default is request.paht
    Returns:
            a popup window
            
    Example usage:
            form = ActivityTypeForm(request.POST or None, instance=activity_type or None)
            form2 = ExtraInfoSettingsForm(request.POST or None, instance=extrainfosettings)
            
            popup = PopUpForm(request, [form, form2], title_text=[_('Wijzig activiteit type'), _(' Wijzig hieronder de weergave instellingen voor overige informatie')], url_address=[request.path, request.path])
            ## The PopUpForm will automatically detect if the request was a POST or something else (GET). The popup.is_valid() method will only be true if all the individual form.is_valid()'s are true!
            if popup.is_valid():      
                # be carefull with saving!!!
                activity_type=form.save()
                extrainfosettings=form2.save(commit=False)             
                extrainfosettings.activity_type = activity_type
                extrainfosettings.save()
                #!!!! the argument in popup.render( XxX ) in the post of a multiform should be one of the created/modified instance types... reaction of the form (succes_created or succes_modified) will depend on it!
                
            return popup.render(activity_type)
    """
    
    def __init__(self, request, form_list, **kwargs):
        # initialisations are either lists/tuples or instances
        self.field = kwargs.get('field', [])
        self.title_text = kwargs.get('title_text', [])
        self.url_address = kwargs.get('url_address', [])
        self.extra_text = kwargs.get('extra_text', [])
        self.custom_save = kwargs.get('custom_save', False) # for multiple_forms always implement your own custom save!!!
        self.extra_js = kwargs.get('extra_js', [])
        self.remove_submit_button = kwargs.get('remove_submit_button', False)

        self.multiple_submit = kwargs.get('multiple_submit', True) #Add False if you want only one submit button with multiple forms

        self.delete_button = kwargs.get('delete_button', False) # if you want an extra delete button, only if model instance exists!
        self.submit_text2 = kwargs.get('submit_text2', False) # If you want an extra submit button
        
        self.is_ajax = request.is_ajax()  # Boolean    
        self.valid_bool = False    
        self.form_list = form_list
        self.request = request
        self.custom_template = kwargs.get('custom_template', None)
        
        if isinstance(form_list, (list, tuple)):
            if len(form_list) > 1:
                self.multiple_forms=True
                self.length=len(form_list)
            else:
                self.multiple_forms=False
                self.form = form_list[0]
        else:
            self.multiple_forms=False
            self.form = form_list
 
        if not request.POST:
            self.pre_get()
        
        if request.POST:
            self.validate()
            self.pre_save()
    
    def prefix(self, i):
        return 'form_'+force_text(i)
    
    def is_valid(self):
        return self.validate()
            
    def validate(self):
        if self.multiple_forms:
            self.valid_bool = self.multiple_forms_is_valid()
            return self.valid_bool
        else:
            self.valid_bool = self.form.is_valid()
            return self.valid_bool
                
    def pre_get(self):
        #set prefix for multiple_forms
        if self.multiple_forms:
            for i, form in enumerate(self.form_list):
                form.prefix = self.prefix(i)
  
    def pre_save(self):
        if self.valid_bool:
            if self.multiple_forms:
                return self.pre_save_multiple_forms()
            else:
                return self.pre_save_single_form()
            
    def render(self, ModelInstance=None):
        if self.valid_bool:
            if self.request.POST: 
                return self.render_post(ModelInstance)
        else: 
            return self.render_get()
        
    def pre_save_multiple_forms(self):
        self.new_instance = True
        for i, form in enumerate(self.form_list):
            if hasattr(form, 'instance') and form.instance.pk:
                self.new_instance = False
                break

    def pre_save_single_form(self):
        self.new_instance = True

        if hasattr(self.form, 'instance') and self.form.instance.pk:
            self.new_instance = False

    def render_get(self):

        if self.multiple_forms:
            return self.render_get_multiple_forms()
        else:
            return self.render_get_single_form()
        
        
    def render_post(self, ModelInstance=None):
        if self.custom_save or self.multiple_forms:
            return self.render_post_return(ModelInstance)
        elif not self.custom_save:
            return form_save_and_succes(self.request, self.form, self.is_ajax)


    def render_get_multiple_forms(self):
        add=False
        userinputs = [self.field, self.title_text, self.url_address, self.extra_text, self.multiple_submit]
        '''
        for userinput in userinputs:
            # first check if is isntance list or tuple:
            # not used
            
            if isinstance(userinput, (list, tuple)):
                if not len(userinput) == self.length:
                    pass
            else:
                userinput
             
        '''
                
        for i, form in enumerate(self.form_list):
            context = {'submit_text2':self.submit_text2}
            
            if not self.title_text:
                # If one of the instances does not exist, change form to an add form with only one title.
                if hasattr(form, 'instance') and form.instance.pk:  # the form object already exists
                    context.update({'submit_text':_('Wijzig')})
                    try:
                        context.update({'title_text':_('Wijzig {0}: {1}').format(self.field[i], form.instance)})
                        self.title_text[i] = _('Wijzig {0}: {1}').format(self.field[i], form.instance)
                    except:
                        context.update({'title_text':_('Wijzig {0}: {1}').format(self.field[0], form.instance)})
                        self.title_text = _('Wijzig {0}: {1}').format(self.field[0], form.instance)                        

                else:
                    context.update({'title_text':_('Voeg {0} toe').format(self.field[0]), 'submit_text':_('Voeg toe')})
                    if not add:
                        self.title_text = _('Voeg {0} toe').format(self.field[0])
                    else:
                        self.title_text = [None]
                    add=True
                    
            else:
           	    context.update({'title_text':self.title_text, 'submit_text':_('Sla op'), 'multiple_submit':self.multiple_submit})
           	    
            if hasattr(form, 'instance') and form.instance.pk:
                context.update({'delete_button':self.delete_button})
                
        if self.custom_template:
            template = self.custom_template
        elif self.is_ajax:
            template = 'horsestops/form/popupFieldMultiple.html'
        else:
            template = 'horsestops/form/popupFormMultiple.html'
        
        context.update({'form_list': self.form_list})
        
        if self.extra_text:
            context.update({'extra_text': self.extra_text})
        if self.extra_js:
            context.update({'extra_js': self.extra_js})
        if self.remove_submit_button:
            context.update({'remove_submit_button':self.remove_submit_button})
        context.update({'post_to':self.url_address[0]})


        return render(self.request, template, context)
    
    def render_post_return(self, modelInstance):

        if modelInstance and self.is_ajax:
            if self.new_instance:
                return message_response(self.request, 'add', modelInstance)
            else:
                return message_response(self.request, 'change', modelInstance)
            
        elif modelInstance and not self.is_ajax:
            return HttpResponse(
                '<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>' % \
                (escape(modelInstance._get_pk_val()), escapejs(modelInstance)))
        else:
            raise ImproperlyConfigured('The forms probably did not validate well? You should use render_post only for validated forms!\
                                        Also make sure that you have given a modelInstance if you have entered multiple forms!')
    

    def render_get_single_form(self):

        if not self.title_text:
            if hasattr(self.form, 'instance') and self.form.instance.pk:  # the form object already exists
                context = {'title_text':_('Wijzig {0}: {1}').format(self.field, self.form.instance), 'submit_text':_('Wijzig')}
            else:
                context = {'title_text':_('Voeg {0} toe').format(self.field), 'submit_text':_('Voeg toe')}
        else:
            context = {'title_text':self.title_text, 'submit_text':_('Sla op')}
        if self.custom_template:
            template = self.custom_template  
        elif self.is_ajax:
            template = 'horsestops/form/popupField.html'
        else:
            template = 'horsestops/form/popupForm.html'
   
        context.update({'form': self.form,'url_address':self.url_address or self.request.get_full_path()})
        context.update({'post_to':self.url_address or self.request.get_full_path()})
        if self.extra_text:
            context.update({'extra_text': self.extra_text})
        if self.extra_js:
            context.update({'extra_js': self.extra_js})
        if self.remove_submit_button:
            context.update({'remove_submit_button':self.remove_submit_button})
        if hasattr(self.form, 'instance') and self.form.instance.pk:
            context.update({'delete_button':self.delete_button})
        return render(self.request, template, context)
    
    def multiple_forms_is_valid(self):
        found_unvalid_form=False
        for i, form in enumerate(self.form_list):
            # loop over the forms, if one is not valid, return False, otherwise return True
            #import ipdb;ipdb.set_trace()
            form.prefix = self.prefix(i)            
            # Ensure both forms are valid before continuing on
            try:
                if not form.is_valid():
                    found_unvalid_form = True
            except:
                found_unvalid_form = True
                
        if found_unvalid_form:
            return False            
        return True     