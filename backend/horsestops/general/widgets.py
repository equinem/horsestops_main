import math
from itertools import chain

from django import forms
from django.utils.html import conditional_escape, format_html
from django.utils.safestring import mark_safe

from django.template.loader import render_to_string

from django.utils.encoding import force_text
import django
from django.core.urlresolvers import reverse_lazy
from django.forms.widgets import Select, MultiWidget, TextInput
from django.utils import translation
from phonenumbers.phonenumberutil import _COUNTRY_CODE_TO_REGION_CODE
from babel.core import Locale
from phonenumber_field.phonenumber import PhoneNumber



if django.VERSION < (1, 9):
    DEFAULT_ADD_ICON = 'admin/img/icon_addlink.gif'
    DEFAULT_EDIT_ICON = 'admin/img/icon_changelink.gif'
else:
    DEFAULT_ADD_ICON = 'admin/img/icon-addlink.svg'
    DEFAULT_EDIT_ICON = 'admin/img/icon-changelink.svg'
   

'''### The widgets! ###'''


class PlainHyperlinkWidget(TextInput):
    '''
    Shows textinput with hyperlink below
    '''   
    def render(self, name, value, attrs=None):

        output = super(PlainHyperlinkWidget, self).render(name, value, attrs)
        output2 = mark_safe('<button type="button"  onclick="$(\'.url-show-button\').toggle();" style="float:right;" class="btn btn-default btn-xs "><span class="glyphicon glyphicon-pencil"></span></button>')
        output1 = mark_safe('<a  href="'+value+'" target="_blank">'+value+'</a>') if value is not None else '-'
        return mark_safe(output) + output1 + output2

class PlainTextWidget(forms.Widget):
    def render(self, name, value, attrs=None):
        if name == 'country':
            return mark_safe(value.name) if value else '-'
        return mark_safe(value) if value else '-'


class MultiStateChoiceInput(forms.widgets.ChoiceInput):
    input_type = 'radio'
    def __init__(self, name, value, attrs, choice, index, label_id):
        # Override to use the label_id which is upped with 1
        if 'id' in attrs:
            self.label_id = attrs['id']+ "_%d" % label_id
            

        super(MultiStateChoiceInput, self).__init__(name, value, attrs, choice, index)

        self.value = force_text(self.value)
        #import ipdb;ipdb.set_trace()
        

    @property
    def id_for_label(self):
        return self.label_id
            
    def render(self, name=None, value=None, attrs=None, choices=()):
        if self.id_for_label:           
            label_for = format_html(' for="{}"', self.id_for_label)
        else:
            label_for = ''
            
            
        attrs = dict(self.attrs, **attrs) if attrs else self.attrs
        
        choice_label = mark_safe('<div></div>')
        return format_html(
            '{} <label{}>{}</label>',  self.tag(attrs), label_for, choice_label
        )


class MultiStateRenderer(forms.widgets.ChoiceFieldRenderer):
    choice_input_class = MultiStateChoiceInput

    outer_html = '<span class="cyclestate" data-tap-disabled="true">{content}</span>'
    inner_html = '{choice_value}{sub_widgets}'

    def render(self):
        """
        Outputs a <ul> for this set of choice fields.
        If an id was given to the field, it is applied to the <ul> (each
        item in the list will get an id of `$id_$i`).
        
        # upgraded with the label_id
        """
        id_ = self.attrs.get('id')
        output = []
        for i, choice in enumerate(self.choices):
            choice_value, choice_label = choice
            if isinstance(choice_label, (tuple, list)):
                attrs_plus = self.attrs.copy()
                if id_:
                    attrs_plus['id'] += '_{}'.format(i)
                sub_ul_renderer = self.__class__(
                    name=self.name,
                    value=self.value,
                    attrs=attrs_plus,
                    choices=choice_label,
                    label_id = (i+1) % (len(self.choices))  # label_id is next one
                )
                sub_ul_renderer.choice_input_class = self.choice_input_class
                output.append(format_html(self.inner_html, choice_value=choice_value,
                                          sub_widgets=sub_ul_renderer.render()))
            else:
                w = self.choice_input_class(self.name, self.value,
                                            self.attrs.copy(), choice, i,  label_id = (i+1) % (len(self.choices)))  # label_id is next one
                output.append(format_html(self.inner_html,
                                          choice_value=force_text(w), sub_widgets=''))
        return format_html(self.outer_html,
                           id_attr=format_html(' id="{}"', id_) if id_ else '',
                           content=mark_safe('\n'.join(output)))


class MultiStateSelectWidget(forms.widgets.RendererMixin, forms.widgets.Select):
    ''' This widget enables multistate clickable toggles 
    Requires some css as well  (see .cyclestate)
    '''
    renderer = MultiStateRenderer
    class Media:
        js = ('js/legacy/cyclestateEdgeFix.js', )



class SelectWithPop(forms.Select):
    """Deze widget maakt een select met een groene plus ernaast,
       welke een popup opent om een nieuwe instance toe te voegen.
    """
    
    def __init__(self, *args, **kwargs):
        if kwargs.pop('add_icon', None) is None:
            add_icon = DEFAULT_ADD_ICON
        if kwargs.pop('edit_icon', None) is None:
            edit_icon = DEFAULT_EDIT_ICON
        super(SelectWithPop, self).__init__(*args, **kwargs)        
        self.edit_icon = edit_icon
        self.add_icon = add_icon


    def render(self, name, value, *args, **kwargs):
        from equinem.templatetags.custom_filters import cutform
        attrs = kwargs.pop('attrs', {})
        attrs.update({'id':"id_%s" % name, "class":"form-control"})
        html = super(SelectWithPop, self).render(name, value, attrs=attrs, *args, **kwargs)
        ctx =  {'field': name,
                'add_url': reverse_lazy('equinem:add_'+cutform(name)), 
                'add_icon': self.add_icon,
                'edit_icon': self.edit_icon}
        popupplus = render_to_string("horsestops/form/popupplus.html", ctx)
        return html+popupplus

class SelectWithAddUrl(forms.Select):
    def __init__(self, *args, **kwargs):
        if kwargs.pop('add_icon', None) is None:
            add_icon = DEFAULT_ADD_ICON
        if kwargs.pop('edit_icon', None) is None:
            edit_icon = DEFAULT_EDIT_ICON
        self.add_related_url = kwargs.pop('add_related_url')
        
        super(SelectWithAddUrl, self).__init__(*args, **kwargs)
        self.edit_icon = edit_icon
        self.add_icon = add_icon


        
    def render(self, name, value, *args, **kwargs):
        attrs = kwargs.pop('attrs', {})
        attrs.update({'id':"id_%s" % name, "class":"form-control"})
        html = super(SelectWithAddUrl, self).render(name, value, attrs=attrs, *args, **kwargs)
        ctx =  {'field': name, 
                'add_icon': self.add_icon,
                'edit_icon': self.edit_icon,
                'add_url': self.add_related_url}
        popupplus = render_to_string("horsestops/form/popupplus.html", ctx )
        return html+popupplus







class ColumnCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    """
    Widget that renders multiple-select checkboxes in columns.
    Constructor takes number of columns and css class to apply
    to the <ul> elements that make up the columns.
    """
    def __init__(self, columns=2, css_class=None, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        self.columns = columns
        self.css_class = css_class

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        choices_enum = list(enumerate(chain(self.choices, choices)))

        # This is the part that splits the choices into columns.
        # Slices vertically.  Could be changed to slice horizontally, etc.
        column_sizes = columnize(len(choices_enum), self.columns)
        columns = []
        for column_size in column_sizes:
            columns.append(choices_enum[:column_size])
            choices_enum = choices_enum[column_size:]
        output = []
        for column in columns:
            if self.css_class:
                output.append(u'<ul class="%s"' % self.css_class)
            else:
                output.append(u'<ul>')
            # Normalize to strings
            str_values = set([str(v) for v in value])
            for i, (option_value, option_label) in column:
                # If an ID attribute was given, add a numeric index as a suffix,
                # so that the checkboxes don't all have the same ID attribute.
                if has_id:
                    final_attrs = dict(final_attrs, id='%s_%s' % (
                            attrs['id'], i))
                    label_for = u' for="%s"' % final_attrs['id']
                else:
                    label_for = ''

                cb = forms.CheckboxInput(
                    final_attrs, check_test=lambda value: value in str_values)
                option_value = str(option_value)
                rendered_cb = cb.render(name, option_value)
                option_label = conditional_escape(str(option_label))
                output.append(u'<li><label%s>%s %s</label></li>' % (
                        label_for, rendered_cb, option_label))
            output.append(u'</ul>')
        return mark_safe(u'\n'.join(output))


def columnize(items, columns):
    """
    Return a list containing numbers of elements per column if `items` items
    are to be divided into `columns` columns.

    >>> columnize(10, 1)
    [10]
    >>> columnize(10, 2)
    [5, 5]
    >>> columnize(10, 3)
    [4, 3, 3]
    >>> columnize(3, 4)
    [1, 1, 1, 0]
    """
    elts_per_column = []
    for col in range(columns):
        col_size = int(math.ceil(float(items) / columns))
        elts_per_column.append(col_size)
        items -= col_size
        columns -= 1
    return elts_per_column


class PhonePrefixSelect(Select):
    initial = None

    def __init__(self, initial=None):
        choices = [('', '---------')]
        language = translation.get_language()
        if language:
            locale = Locale(translation.to_locale(language))
            for prefix, values in _COUNTRY_CODE_TO_REGION_CODE.items():
                
                prefix = '+%d' % prefix


                if initial and initial in values:
                    self.initial = prefix

                choices.append((prefix, u'%s' % (prefix)))
        super(PhonePrefixSelect, self).__init__(
            choices=sorted(choices, key=lambda item: item[1]))

    def render(self, name, value, *args, **kwargs):
        return super(PhonePrefixSelect, self).render(
            name, value or self.initial, *args, **kwargs)


class PhoneNumberPrefixWidget(MultiWidget):
    """
    A Widget that splits phone number input into:
    - a country select box for phone prefix
    - an input for local phone number
    """



    def __init__(self, attrs=None, initial=None):
        widgets = (PhonePrefixSelect(initial), TextInput(),)
        super(PhoneNumberPrefixWidget, self).__init__(widgets, attrs)

    def decompress(self, value):

        if value:
            if type(value) == PhoneNumber:
                if value.country_code and value.national_number:
                    return ["+%d" % value.country_code, value.national_number]
            else:
                return value.split('.')
        return [None, None]

    def value_from_datadict(self, data, files, name):

        values = super(PhoneNumberPrefixWidget, self).value_from_datadict(
            data, files, name)
        if not values[0] and not values[1]:
            return None
        else:
            return '%s.%s' % tuple(values)

