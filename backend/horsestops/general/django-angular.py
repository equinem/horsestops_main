from django.template import loader, RequestContext
from django.http import HttpResponse

def render(request, *args, **kwargs):
    """
    Returns a HttpResponse whose content is filled with the result of calling
    django.template.loader.render_to_string() with the passed arguments.
    Uses a RequestContext by default.
    
    <[ {{ variable }} ]> will be rendered to <[ x.y ]> (assuming variable is "x.y") which will then be transformed to {{ x.y }} which AngularJS can handle.
    
    """
    httpresponse_kwargs = {
    'content_type': kwargs.pop('content_type', None),
    'status': kwargs.pop('status', None),
    }
    if 'context_instance' in kwargs:
        context_instance = kwargs.pop('context_instance')
    if kwargs.get('current_app', None):
        raise ValueError('If you provide a context_instance you must '
                         'set its current_app before calling render()')
    else:
        current_app = kwargs.pop('current_app', None)
        context_instance = RequestContext(request, current_app=current_app)

    kwargs['context_instance'] = context_instance
    s = loader.render_to_string(*args, **kwargs)
    s = s.replace("<[", "{{").replace("]>", "}}") # <- magic happens *here*

    return HttpResponse(s, **httpresponse_kwargs)