from django.shortcuts import HttpResponse
import json
from django.shortcuts import render_to_response
from django.template import RequestContext

# `data` is a python dictionary
def render_to_json(request, data):
    return HttpResponse(
        json.dumps(data, ensure_ascii=False),
        mimetype=request.is_ajax() and "application/json" or "text/html"
    )
    
def render_with(template):
    def render_with_decorator(view_func):
        def wrapper(*args, **kwargs):
            request = args[0]
            context = view_func(*args, **kwargs)
            return render_to_response(
                template, 
                context, 
                context_instance=RequestContext(request),
            )
        return wrapper
    return render_with_decorator