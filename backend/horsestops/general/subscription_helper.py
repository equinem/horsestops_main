from billing.equinem_members.models import StableMerchantId
from django.conf import settings
from horsestops.general.helper import EU_COUNTRY_LIST
import braintree
from decimal import Decimal


def get_subscription(sub_type, stable):
    reverse_charged = False
    if sub_type ==  StableMerchantId.HORSELOVER:
        plan_id = settings.HORSELOVER_PLAN_ID
        extra_horse_id = settings.HORSELOVER_EXTRA_HORSE_ID
        vat_included = True
        extra_horses = max(0, stable.max_no_horses - settings.HORSELOVER_HORSES_INCLUDED_IN_PLAN)
    elif sub_type ==  StableMerchantId.PROFESSIONAL:
        if stable.country in EU_COUNTRY_LIST and stable.country != 'NL' and stable.vat_number:
            # do not charge VAT
            vat_included = False
            reverse_charged = True
            plan_id = settings.PROFESSIONAL_WITHOUT_VAT_PLAN_ID
            extra_horse_id = settings.PROFESSIONAL_WITHOUT_VAT_EXTRA_HORSE_ID
        else:
            plan_id = settings.PROFESSIONAL_WITH_VAT_PLAN_ID
            extra_horse_id = settings.PROFESSIONAL_WITH_VAT_EXTRA_HORSE_ID
            vat_included = True
        extra_horses = max(0, stable.max_no_horses - settings.PROFESSIONAL_HORSES_INCLUDED_IN_PLAN)

    elif sub_type == StableMerchantId.ENTERPRISE:
        if stable.country in EU_COUNTRY_LIST and stable.country != 'NL' and stable.vat_number:
            # do not charge VAT
            vat_included = False
            reverse_charged = True
            plan_id = settings.ENTERPRISE_WITHOUT_VAT_PLAN_ID
            extra_horse_id = settings.ENTERPRISE_WITHOUT_VAT_EXTRA_HORSE_ID
        else:
            plan_id = settings.ENTERPRISE_WITH_VAT_PLAN_ID
            extra_horse_id = settings.ENTERPRISE_WITH_VAT_EXTRA_HORSE_ID
            vat_included = True
            
        extra_horses = max(0, stable.max_no_horses - settings.ENTERPRISE_HORSES_INCLUDED_IN_PLAN)

    
    return plan_id, extra_horse_id, vat_included, reverse_charged, extra_horses
    
def get_subscription_costs(sub_type, stable):
    # Will get the subscription costs from braintree, so you only need to update there!
    
    plan_id, extra_horse_id, vat_included, reverse_charged, extra_horses = get_subscription(sub_type, stable)
    
    plans = braintree.Plan.all()
    for plan in plans:
        if plan.id == plan_id:
            costs = plan.price
            break
        
    if extra_horses:
        add_ons = braintree.AddOn.all()
        for add_on in add_ons:
            if add_on.id == extra_horse_id:
                costs = Decimal(costs) + Decimal(extra_horses) * add_on.amount
                
    return costs, vat_included, reverse_charged
    
