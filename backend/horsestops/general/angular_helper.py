from django.views.generic.base import TemplateView
#from djangular.core.urlresolvers import urls_by_namespace


class PartialGroupView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(PartialGroupView, self).get_context_data(**kwargs)
        # update the context
        #import ipdb;ipdb.set_trace()
        
        return context
    
    
#def get_partials():
#    my_partials = urls_by_namespace('partials')
#    return my_partials