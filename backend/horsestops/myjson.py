import json
from time import mktime
import datetime
from kombu.utils.encoding import bytes_to_str
import decimal

# below DecimalEncoder
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)

        return super(DecimalEncoder, self).default(o)


# below the encoder used for rabbitmq/celery
class MyEncoder(json.JSONEncoder):   
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {
                '__type__': '__datetime__', 
                'epoch': int(mktime(obj.timetuple()))
            }
        elif isinstance(obj, datetime.date):
            return {
                '__type__': '__date__', 
                'iso-8601': str(obj.isoformat())
            }
        else:
            return json.JSONEncoder.default(self, obj)

def my_decoder(obj):
    if '__type__' in obj:
        if obj['__type__'] == '__datetime__':
            return datetime.datetime.fromtimestamp(obj['epoch'])
        if obj['__type__'] == '__date__':
            return datetime.datetime.strptime(obj['iso-8601'], "%Y-%m-%d").date()
    return obj

# Encoder function      
def my_dumps(obj):
    return json.dumps(obj, cls=MyEncoder)

# Decoder function
def my_loads(obj):
    return json.loads(bytes_to_str(obj), object_hook=my_decoder)