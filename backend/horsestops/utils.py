from django.utils.encoding import smart_text
from wkhtmltopdf.utils import make_absolute_paths, render_to_temporary_file,\
    convert_to_pdf
from django.core.files.temp import NamedTemporaryFile
from django.conf import settings
from wkhtmltopdf.views import PDFResponse
from django.template import loader
import six


def print_html_to_file(content, mode='w+b', bufsize=-1,
                                 suffix='.html', prefix='tmp', dir=None,
                                 delete=True):
    
    # content = response.content
    content = smart_text(content)
    content = make_absolute_paths(content)

    try:
        # Python3 has 'buffering' arg instead of 'bufsize'
        tempfile = NamedTemporaryFile(mode=mode, buffering=bufsize,
                                      suffix=suffix, prefix=prefix,
                                      dir=dir, delete=delete)
    except TypeError:
        tempfile = NamedTemporaryFile(mode=mode, bufsize=bufsize,
                                      suffix=suffix, prefix=prefix,
                                      dir=dir, delete=delete)

    try:
        tempfile.write(content.encode('utf-8'))
        tempfile.flush()
        return tempfile
    except:
        # Clean-up tempfile if an Exception is raised.
        tempfile.close()
        raise

def render_pdf_from_content(content, header_template, footer_template, context, cmd_options=None):
    debug = getattr(settings, 'WKHTMLTOPDF_DEBUG', settings.DEBUG)
    cmd_options = cmd_options if cmd_options else {}

    input_file = header_file = footer_file = None
    header_filename = footer_filename = None

    try:
        input_file = print_html_to_file(
            content=content,
            prefix='wkhtmltopdf', suffix='.html',
            delete=(not debug)
        )

        if header_template:
            header_file = render_to_temporary_file(
                template=header_template,
                context=context,
                prefix='wkhtmltopdf', suffix='.html',
                delete=(not debug)
            )
            header_filename = header_file.name

        if footer_template:
            footer_file = render_to_temporary_file(
                template=footer_template,
                context=context,
                prefix='wkhtmltopdf', suffix='.html',
                delete=(not debug)
            )
            footer_filename = footer_file.name

        return convert_to_pdf(filename=input_file.name,
                              header_filename=header_filename,
                              footer_filename=footer_filename,
                              cmd_options=cmd_options)
    finally:
        # Clean up temporary files
        for f in filter(None, (input_file, header_file, footer_file)):
            f.close()

class PDFContentResponse(PDFResponse):
    """Renders a Template into a PDF using wkhtmltopdf"""

    def __init__(self, request, content, context=None,
                 status=None, content_type=None, current_app=None,
                 filename=None, show_content_in_browser=None,
                 header_template=None, footer_template=None,
                 cmd_options=None, *args, **kwargs):

        super(PDFContentResponse, self).__init__(request=request,
                                                  content=content,
                                                  context = context,
                                                  status=status,
                                                  content_type=content_type,
                                                  current_app=None,
                                                  *args, **kwargs)
        self.set_filename(filename, show_content_in_browser)
        self.header_template = header_template
        self.footer_template = footer_template
        self._is_rendered = False
        self._post_render_callbacks = []
        self.using = None
        self.context_data = context
        if cmd_options is None:
            cmd_options = {}
        self.cmd_options = cmd_options

    @property
    def rendered_content(self):
        """Returns the freshly rendered content for the template and context
        described by the PDFResponse.

        This *does not* set the final content of the response. To set the
        response content, you must either call render(), or set the
        content explicitly using the value of this property.
        """
        cmd_options = self.cmd_options.copy()
        return render_pdf_from_content(
            self.content,
            self.resolve_template(self.header_template),
            self.resolve_template(self.footer_template),
            context=self.resolve_context(self.context_data),
            cmd_options=cmd_options
        )

    def render(self):
        """Renders (thereby finalizing) the content of the response.

        If the content has already been rendered, this is a no-op.

        Returns the baked response instance.
        """
        retval = self
        if not self._is_rendered:
            self.content = self.rendered_content
            for post_callback in self._post_render_callbacks:
                newretval = post_callback(retval)
                if newretval is not None:
                    retval = newretval
        return retval

    def resolve_template(self, template):
        "Accepts a template object, path-to-template or list of paths"
        if isinstance(template, (list, tuple)):
            return loader.select_template(template, using=self.using)
        elif isinstance(template, six.string_types):
            return loader.get_template(template, using=self.using)
        else:
            return template
        
    def resolve_context(self, context):
        return context
