from django.forms import ModelChoiceField, ModelMultipleChoiceField
from django.contrib.auth import get_user_model
from django import forms

class UserModelChoiceField(ModelChoiceField):
    ''' 
    A ModelChoiceField to represent User 
    select boxes in the Auto Admin 
    '''
    def label_from_instance(self, obj):
        return "%s (%s)"%(obj.get_full_name(), obj.username)

class UserModelMultipleChoiceField(ModelMultipleChoiceField):
    ''' 
    Similar to UserModelChoiceField, provide a nicer-looking 
    list of user names for ManyToMany Relations...
    '''
    def label_from_instance(self, obj):
        return "%s (%s)"%(obj.get_full_name(), obj.username)


class UserAdminForm(forms.ModelForm):
    User=get_user_model()
    user = UserModelChoiceField(User.objects.all().order_by('first_name', 'last_name', 'username'))   
 