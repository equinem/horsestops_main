# DRF has no authentication middleware like with the sessions. Therefore created my own for tokens and JWT (the only used ones).


from rest_framework.request import Request
from django.utils.functional import SimpleLazyObject
from django.contrib.auth.middleware import get_user
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import TokenAuthentication
from .utils import PDFContentResponse

from django.utils.translation import gettext as _
import re
from django.conf import settings
import json
from django.utils.deprecation import MiddlewareMixin

class WebFactionFixes(MiddlewareMixin):
    """Sets 'REMOTE_ADDR' based on 'HTTP_X_FORWARDED_FOR', if the latter is
    set.

    Based on http://djangosnippets.org/snippets/1706/
    """
    def process_request(self, request):
        if 'HTTP_X_FORWARDED_FOR' in request.META:
            ip = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
            request.META['REMOTE_ADDR'] = ip


def get_user_jwt(request):
    user = get_user(request)
    if user.is_authenticated():
        return user
    try:
        user_jwt = JSONWebTokenAuthentication().authenticate(Request(request))
        if user_jwt is not None:
            return user_jwt[0]
    except:
        pass
    return user # AnonymousUser


class AuthenticationMiddlewareJWT(MiddlewareMixin):
    def process_request(self, request):
        request.user = SimpleLazyObject(lambda: get_user_jwt(request))
        
        
        
def get_user_token(request):
    user = get_user(request)
    if user.is_authenticated():
        return user
    try:
        user_token = TokenAuthentication().authenticate(Request(request))
        if user_token is not None:
            return user_token[0]
        
    except:
        pass
    
    return user     # AnonymousUser


class AuthenticationMiddlewareToken(MiddlewareMixin):
    def process_request(self, request):
        request.user = SimpleLazyObject(lambda: get_user_token(request))
        
from raven import Client

client = Client(settings.RAVEN_CONFIG['dsn'])
class SetSentryContextMiddleware(MiddlewareMixin):
    def process_request(self, request):
            if hasattr(request,'stable'):
                client.context.merge({'user': {
                'stable': request.stable
            }})



class PrintMiddleWare(MiddlewareMixin):
    '''
    Makes it possible to print a page without creating a seperate view
    
    Example button:
        <form class="not-print" action = "" method = "post">
        {% csrf_token %}
            <button class="btn btn-default not-print" type="submit" name="print" value="margin-left:10mm, margin-right:10mm, margin-top:10mm"><span class='glyphicon glyphicon-print'></span>  {% trans 'Print overzicht' %}</button>
        </form>
    
    '''
    


    def process_response(self, request, response):
        if 'print' in request.POST:
            if hasattr(settings, 'PRINT_TO_HTML') and settings.PRINT_TO_HTML:
                response = HttpResponse(response)
            else:
            
                # Create the HttpResponse object with the appropriate PDF headers.
                if hasattr(request, 'filename'):
                    filename = request.filename
                else:
                    filename = _('Schedule.pdf').format()
                
                cmd_options = {     'margin-top': '6mm',
                                    'margin-bottom': '20mm',
                                    'margin-left': '6mm',
                                    'margin-right': '6mm',
                                    'page-size': 'A4',
                                    'viewport-size': '1280x1024',
                                    'orientation': 'Portrait',
                                    #'disable-javascript': False, 
                                    }
                
                parsed_cmd_options = re.findall(r"[\w'-]+", request.POST.get('print'))
    
                parsed_cmd_options_dict = dict(zip(parsed_cmd_options[0::2], parsed_cmd_options[1::2]))
                
                cmd_options.update(parsed_cmd_options_dict)

                response = PDFContentResponse(request=request,
                                                   content=response,
                                                   filename=filename,
                                                   footer_template='framework/print/print_footer.html',
                                                   show_content_in_browser=True,
                                                   cmd_options=cmd_options,
                                                   ).render()
            

        return response




def print_request(request):
    # sets the context variables
    if 'print' in request.POST:
        context={'LANGUAGE_CODE': request.user.profile.language, 'print':True}

        return context
    return {}


def FAV_ICON_LOCATION(request):
    return {'FAV_ICON_LOCATION': settings.FAV_ICON_LOCATION}
    
    

from django.http import HttpResponse


class HealthCheck(MiddlewareMixin):
    '''
    Adds the /_health/ endpoint for health checking.
    
    Also add ?full for a list of problems.
    '''
    
    def process_request(self, request):
        # Our health check can't be a done as a view, because we need
        # to bypass the ALLOWED_HOSTS check. We need to do this
        # since not all load balancers can send the expected Host header
        # which would cause a 400 BAD REQUEST, marking the node dead.
        # Instead, we just intercept the request at this point, and return
        # our success/failure immediately.
        if request.path != '/_health/':
            return

        if 'full' not in request.GET:
            return HttpResponse('ok', content_type='text/plain')
        from status_checks import check_all
        problems, checks = check_all()

        return HttpResponse(json.dumps({
            'problems': map(str, problems),
            'healthy': checks,
        }), content_type='application/json', status=(500 if problems else 200))


