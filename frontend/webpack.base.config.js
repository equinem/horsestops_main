var path = require("path")
var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports =   {
    	  name: 'index',
	  context: __dirname,

	  entry: {
	  	index: ['./assets/js/index'],
	  	dashboard: ['./assets/js/dashboard']
	  }, 
	      
	  output: {
	      path: path.resolve('./assets/bundles/'),
	      filename: "[name]-[hash].js"
	  },

	  plugins: [
	    new ExtractTextPlugin('[name]-[hash].css'),
	        new webpack.ProvidePlugin({
		    $: "jquery",
		    jQuery: "jquery",
	            "window.jQuery": "jquery"
		})
	  ], // add all common plugins here

	  module: {
	    rules: [      
	      {
		test: /\.scss$/,
		exclude: ['node_modules', 'bower_components'],
		loader: ExtractTextPlugin.extract({
			fallback: 'style-loader', // The backup style loader
		        use: 'css-loader!sass-loader'
		 })

	      },
	      { test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100' },

            { test: /jquery[\\\/]src[\\\/]selector\.js$/, loader: 'amd-define-factory-patcher-loader' },
            { test: /[\/\\]node_modules[\/\\]some-module[\/\\]index\.js$/,
              loader: "imports-loader?this=>window" }
	    ] // add all common loaders here
	  },

	  resolve: {
	    modules: ['node_modules', 'bower_components'], //, path.resolve(__dirname, "src")],
	    extensions: ['.js','.jsx', '.scss'], //, '.css'],
		alias: {
		    jquery: "jquery/src/jquery"
		}
	  }
};

