import '../css/index.scss'; // Use this only for style on the index/main/landing pages.


import 'jquery';
import 'jquery-ui/ui/effect';
import 'bootstrap';
import 'jquery.easing';
import WOW from 'imports-loader?this=>window!exports-loader?this.WOW!wowjs';
import '../js/themescript.js';

