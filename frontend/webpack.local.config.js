var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.base.config.js')

// the ip-address below have to be changed to localhost for local development, or the ip of the machine running django for remote development

var server_ip = 'http://0.0.0.0'

// Use webpack dev server
Object.keys(config.entry).forEach(function(key){
  	config.entry[key].unshift(
	  'react-hot-loader/patch',
	  'webpack-dev-server/client?'+ server_ip +':3000',
	  'webpack/hot/only-dev-server'
	)
});



// override django's STATIC_URL for webpack bundles
config.output.publicPath = server_ip + ':3000/assets/bundles/'

// Add HotModuleReplacementPlugin and BundleTracker plugins
config.plugins = config.plugins.concat([
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  // prints more readable module names in the browser console on HMR updates
  new webpack.NoEmitOnErrorsPlugin(),  // not necessary anymore
  new BundleTracker({filename: './webpack-stats.json'}),
])

// Add a loader for JSX files with react-hot enabled
config.module.rules.push(
  { test: /\.jsx$/, exclude: /node_modules/, loaders: ['react-hot-loader/webpack', 'babel-loader'] }
)


module.exports = config
